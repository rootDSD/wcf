﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MiServicio
{
    [ServiceContract] 
    interface IServicioLibros
    {
        [OperationContract]
        void AdicionarLibros(string nombre);
        [OperationContract]
        void RemoverLibros(string nombre);
        [OperationContract]
        string[] ListaLibros();
    }
}
