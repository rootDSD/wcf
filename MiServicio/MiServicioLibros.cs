﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MiServicio
{
    class MiServicioLibros : IServicioLibros
    {
        static List<string> lista = new List<string>();
        #region IServicioLibros Members
        public void AdicionarLibros(string nombre)
        { 
            lista.Add(nombre);
        }      

        public void RemoverLibros(string nombre)
        {
            lista.Remove(nombre);
        }

        public string[] ListaLibros()
        {
            return lista.ToArray();
        }

        #endregion
    }
}
